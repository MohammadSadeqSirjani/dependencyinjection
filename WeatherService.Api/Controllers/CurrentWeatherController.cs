﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using WeatherService.Api.Models;

namespace WeatherService.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrentWeatherController : ControllerBase
    {
        private readonly IMemoryCache _memoryCache;

        public CurrentWeatherController(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        [HttpGet("{city}")]
        public async Task<ActionResult<WeatherResult>> Get([FromRoute] string city)
        {
            var random = new Random();

            //simulate a slow API response
            await Task.Delay(random.Next(100, 300));

            if (_memoryCache.TryGetValue(city, out var weather))
            {
                if (weather is WeatherResult result)
                {
                    return result;
                }
            }

            //create some random weather
            var condition = random.Next(1, 4);

            var currentWeather = condition switch
            {
                1 => new WeatherResult()
                {
                    City = city,
                    Weather = new WeatherCondition()
                    {
                        Description = "Sun",
                        Wind = new Wind() { Degrees = 190, Speed = 6 },
                        Temperature = new Temperature() { Max = 32, Min = 26 }
                    }
                },
                2 => new WeatherResult()
                {
                    City = city,
                    Weather = new WeatherCondition()
                    {
                        Description = "Rain",
                        Wind = new Wind() { Degrees = 80, Speed = 3 },
                        Temperature = new Temperature() { Max = 14, Min = 8 }
                    }
                },
                3 => new WeatherResult()
                {
                    City = city,
                    Weather = new WeatherCondition()
                    {
                        Description = "Cloud",
                        Wind = new Wind() { Degrees = 10, Speed = 1 },
                        Temperature = new Temperature() { Max = 18, Min = 13 }
                    }
                },
                _ => new WeatherResult()
                {
                    City = city,
                    Weather = new WeatherCondition()
                    {
                        Description = "Snow",
                        Wind = new Wind() { Degrees = 240, Speed = 8 },
                        Temperature = new Temperature() { Max = 1, Min = -3 }
                    }
                }
            };

            _memoryCache.Set(city, currentWeather,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(60 * 12)));

            return currentWeather;
        }
    }
}