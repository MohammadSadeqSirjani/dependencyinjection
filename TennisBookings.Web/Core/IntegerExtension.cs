﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TennisBookings.Web.Core
{
    public static class IntegerExtension
    {
        public static string To12HourClockString(this int hour)
        {
            var suffix = hour < 12 ? "am" : "pm";

            var finalHour = hour;

            if (hour == 0)
            {
                finalHour = 12;
            }
            else if (hour > 12)
            {
                finalHour = hour - 12;
            }

            return $"{finalHour} {suffix}";
        }
    }
}
