﻿using Microsoft.Extensions.DependencyInjection;
using TennisBookings.Web.Services;

namespace TennisBookings.Web.Core.DependencyInjection
{
    public static class BookingServiceCollectionExtensions
    {
        public static IServiceCollection AddBookingServices(this IServiceCollection services)
        {
            services.AddScoped<ICourtService, CourtService>();
            services.AddScoped<ICourtBookingManager, CourtBookingManager>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<ICourtBookingService, CourtBookingService>();

            return services;
        } 
    }
}
