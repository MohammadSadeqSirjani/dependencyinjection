﻿namespace TennisBookings.Web.Configuration
{
    public class BookingConfiguration : IBookingConfiguration
    {
        public int MaxPeakBookingLengthInHours { get; set; }
        public int MaxRegularBookingLengthHours { get; set; }
    }
}
