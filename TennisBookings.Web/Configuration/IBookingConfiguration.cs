﻿namespace TennisBookings.Web.Configuration
{
    public interface IBookingConfiguration
    {
        public int  MaxPeakBookingLengthInHours { get; set; }

        public int MaxRegularBookingLengthHours { get; set; }
    }
}
