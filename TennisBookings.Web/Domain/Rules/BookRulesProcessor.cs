﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TennisBookings.Web.Data;

namespace TennisBookings.Web.Domain.Rules
{
    public class BookRulesProcessor : IBookingRuleProcessor
    {
        private readonly IEnumerable<ICourtBookingRule> _rules;

        public BookRulesProcessor(IEnumerable<ICourtBookingRule> rules)
        {
            _rules = rules;
        }

        public async Task<(bool, IEnumerable<string>)> PassesAllRulesAsync(CourtBooking courtBooking)
        {
            var passRules = true;

            var errors = new List<string>();

            foreach (var rule in _rules)
            {
                if (await rule.CompilesWithRuleAsync(courtBooking)) continue;
                errors.Add(rule.ErrorMessage);
                passRules = false;
            }

            return (passRules, errors);
        }
    }
}
