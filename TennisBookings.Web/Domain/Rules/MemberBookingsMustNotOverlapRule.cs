﻿using System.Linq;
using System.Threading.Tasks;
using TennisBookings.Web.Data;
using TennisBookings.Web.Services;

namespace TennisBookings.Web.Domain.Rules
{
    public class MemberBookingsMustNotOverlapRule : IScopedCourtBookingRule
    {
        private readonly ICourtBookingService _courtBookingService;

        public MemberBookingsMustNotOverlapRule(ICourtBookingService courtBookingService)
        {
            _courtBookingService = courtBookingService;
        }

        public async Task<bool> CompilesWithRuleAsync(CourtBooking courtBooking)
        {
            var todayBookings = (await _courtBookingService
                    .MemberBookingsForDayAsync(courtBooking.StartDateTime.Date, courtBooking.Member))
                .ToArray();

            if (!todayBookings.Any())
                return true; // no booking, so can't be overlap

            var bookingHours = Enumerable.Range(courtBooking.StartDateTime.Hour,
                courtBooking.EndDateTime.Hour - courtBooking.StartDateTime.Hour).ToArray();

            foreach (var existingBooking in todayBookings)
            {
                for (var hour = existingBooking.StartDateTime.Hour; hour < existingBooking.EndDateTime.Hour; hour++)
                {
                    if (bookingHours.Contains(hour))
                        return false;
                }
            }

            return true;

        }

        public string ErrorMessage => "You can't make two bookings which overlap.";
    }
}
