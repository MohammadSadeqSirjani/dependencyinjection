﻿using System;
using System.Threading.Tasks;
using TennisBookings.Web.Configuration;
using TennisBookings.Web.Data;

namespace TennisBookings.Web.Domain.Rules
{
    /// <summary>
    /// A rule which prevents a single booking being longer that the configured max booking.
    /// </summary>
    public class MaxBookingLengthRule : ISingletonCourtBookingRule
    {
        private readonly IBookingConfiguration _bookingConfiguration;

        public MaxBookingLengthRule(IBookingConfiguration bookingConfiguration)
        {
            _bookingConfiguration = bookingConfiguration;
        }

        public Task<bool> CompilesWithRuleAsync(CourtBooking booking)
        {
            var bookingLength = booking.EndDateTime - booking.StartDateTime;

            var compilesWithRule =
                bookingLength <= TimeSpan.FromHours(_bookingConfiguration.MaxRegularBookingLengthHours);

            return Task.FromResult(compilesWithRule);
        }

        public string ErrorMessage => "Booking is longer than allowed length";
    }
}
