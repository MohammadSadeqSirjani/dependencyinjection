﻿namespace TennisBookings.Web.Domain
{
    public class MembershipAdvert : IMembershipAdvert
    {
        public MembershipAdvert(decimal offerPrice, decimal saving)
        {
            OfferPrice = offerPrice;
            Saving = saving;
        }

        public decimal OfferPrice { get; set; }
        public decimal Saving { get; set; }
    }
}
