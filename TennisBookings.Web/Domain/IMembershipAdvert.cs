﻿namespace TennisBookings.Web.Domain
{
    public interface IMembershipAdvert
    {
        public decimal OfferPrice { get; set; }

        public decimal Saving { get; set; }
    }
}
