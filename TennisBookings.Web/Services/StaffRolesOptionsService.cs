﻿using System.Collections.Generic;

namespace TennisBookings.Web.Services
{
    public class StaffRolesOptionsService : IStaffRolesOptionsService
    {
        public IEnumerable<string> Roles { get; } = new List<string>()
        {
            "Accounts Manger",
            "Cleaner",
            "Club Manager",
            "Coach",
            "Court Maintenance Worker",
            "Receptionist"
        };
    }
}
