﻿using System.Collections.Generic;

namespace TennisBookings.Web.Services
{
    public interface IStaffRolesOptionsService
    {
        IEnumerable<string> Roles { get; }
    }
}
