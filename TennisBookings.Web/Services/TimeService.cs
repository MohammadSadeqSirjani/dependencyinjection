﻿using System;
using Microsoft.Extensions.Logging;

namespace TennisBookings.Web.Services
{
    public interface ITimeService
    {
        DateTime CurrentTime { get; set; }
    }

    public interface IUtcTimeService
    {
        DateTime CurrentUtcTime { get; set; }
    }

    public class TimeService: ITimeService, IUtcTimeService
    {
        private readonly ILogger<TimeService> _logger;

        public TimeService(ILogger<TimeService> logger)
        {
            _logger = logger;
        }

        public TimeService()
        {
            var guid = Guid.NewGuid();

            _logger.LogInformation($"TimeService initialized: {guid}");
        }

        public DateTime CurrentTime { get; set; }
     
        public DateTime CurrentUtcTime { get; set; }
    }
}
