﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;

namespace TennisBookings.Web.Services
{
    public class GreetingService : IHomePageGreetingService, IGreetingService
    {
        public string[] Greetings { get; }

        public string[] LoginGreeting { get; }

        private static readonly ThreadLocal<Random> Random =
            new ThreadLocal<Random>(() => new Random());

        public GreetingService(IWebHostEnvironment hostingEnvironment)
        {
            var webRootPath = hostingEnvironment.WebRootPath;

            var greetingJson = File.ReadAllText(webRootPath + "/greeting.json");

            var greetingData = JsonConvert.DeserializeObject<GreetingData>(greetingJson);

            Greetings = greetingData.Greetings;

            LoginGreeting = greetingData.LoginGreetings;

        }

        [Obsolete("Prefer the GetRandomGreeting method defined in IGreetingService")]
        public string GetRandomHomePageGreeting()
        {
            return GetRandomGreeting();
        }

        public string GetRandomGreeting()
        {
            return GetRandomValue(Greetings);
        }

        public string GetRandomLoginGreeting(string name)
        {
            var loginGreeting = GetRandomValue(LoginGreeting);

            return loginGreeting.Replace("{name}", name);
        }

        private string GetRandomValue(IReadOnlyList<string> greetings)
        {
            if (greetings.Count == 0) return string.Empty;

            var greetingToUse = Random.Value.Next(greetings.Count);

            return greetingToUse >= 0 ? greetings[greetingToUse] : string.Empty;
        }

        private class GreetingData
        {
            public string[] Greetings { get; set; }

            public string[] LoginGreetings { get; set; }
        }
    }
}
