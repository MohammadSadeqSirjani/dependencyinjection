﻿namespace TennisBookings.Web.Auditing
{
    public class DatabaseAuditor : IAuditor
    {
        public string SourceName { get; set; }

        public DatabaseAuditor(string sourceName)
        {
            SourceName = sourceName;
        }


        public void RecordAction(string message)
        {
            //imagine writing audit data to a database
        }
    }
}
