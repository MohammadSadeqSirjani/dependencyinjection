﻿namespace TennisBookings.Web.Auditing
{
    public interface IAuditor<T> : IAuditor
    {

    }

    public interface IAuditor
    {
        void RecordAction(string message);
    }
}
