﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TennisBookings.Web.Core;
using TennisBookings.Web.Data;
using TennisBookings.Web.Services;

namespace TennisBookings.Web.Pages
{
    public class FindAvailableCourtsModel : PageModel
    {
        private readonly IBookingService _bookingService;

        public FindAvailableCourtsModel(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        public async Task OnGet()
        {
            if (SearchDate == default)
                SearchDate = DateTime.UtcNow.Date;

            await LoadAvailability();
        }

        public IActionResult OnPost()
        {
            return RedirectToPage("FindAvailableCourts", new
            {
                searchDate = SearchDate.ToString("yyyy-MM-dd")
            });
        }

        private async Task LoadAvailability()
        {
            if (SearchDate.Date < DateTime.UtcNow.Date)
            {
                Availability = Array.Empty<TableData>();
                return;
            }

            var availability = await _bookingService.GetBookingAvailabilityForeDateAsync(SearchDate);

            var tableData = new List<TableData>(24);

            var foundFirstRow = false;

            var lastHourWithAvailability = 23;

            foreach (var (key, value) in availability)
            {
                var noAvailability = !value.ContainsValue(true);

                if (noAvailability && !foundFirstRow)
                    continue;

                foundFirstRow = true;

                if (!noAvailability)
                    lastHourWithAvailability = key;

                var data = new TableData(key, SearchDate, value);

                tableData.Add(data);
            }

            Availability = tableData.Where(x => x.Hour <= lastHourWithAvailability);
        }

        [BindProperty(SupportsGet = true)] public DateTime SearchDate { get; set; }

        public IEnumerable<TableData> Availability { get; set; }

        public bool HasNoAvailability => !Availability.Any();

        public class TableData
        {
            public int Hour { get; set; }

            public string HourText { get; set; }

            public string BookingsStartDate { get; set; }

            public IEnumerable<CourtData> CourtAvailability { get; set; }

            public TableData(int hour, DateTime date, Dictionary<int, bool> courtAvailability)
            {
                if (hour < 0 || hour > 23)
                    throw new ArgumentOutOfRangeException(nameof(hour));

                if (courtAvailability == null)
                    throw new ArgumentNullException(nameof(courtAvailability));

                Hour = hour;
                HourText = hour.To12HourClockString();
                BookingsStartDate = date.Date.AddHours(hour).ToString("O");
                CourtAvailability = courtAvailability.Select(v => new CourtData()
                {
                    CourtId = v.Key,
                    Available = v.Value
                });
            }
        }

        public class CourtData
        {
            public int CourtId { get; set; }

            public bool Available { get; set; }
        }
    }
}