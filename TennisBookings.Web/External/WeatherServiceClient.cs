﻿using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TennisBookings.Web.Configuration;
using TennisBookings.Web.External.Models;

namespace TennisBookings.Web.External
{
    public class WeatherApiClient : IWeatherApiClient
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<WeatherApiClient> _logger;

        public WeatherApiClient(HttpClient httpClient, IOptions<ExternalServicesConfig> options, ILogger<WeatherApiClient> logger)
        {
            var externalServicesConfig = options.Value;

            _httpClient = httpClient;
            _logger = logger;
        }

        public async Task<WeatherApiResult> GetWeatherForecastAsync()
        {
            const string path = "api/currentweather/Mashhad";

            try
            {
                var response = await _httpClient.GetAsync(path);

                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }

                var content = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<WeatherApiResult>(content);
            }
            catch (HttpRequestException e)
            {
                _logger.LogError(e,"Fail to get weather data from API");
            }

            return null;
        }
    }
}
