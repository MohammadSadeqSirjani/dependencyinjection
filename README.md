High-level modules should not depend on low-level modules.Both should depend on 
abstractions.
Abstractions should not depend upon details.Details should depend upon abstractions.
Because of that we use dependency injection pattern