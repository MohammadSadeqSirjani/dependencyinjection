﻿using Microsoft.AspNetCore.Mvc;
using TennisBookings.Web.Controllers;
using TennisBookings.Web.ViewModels;
using Xunit;
using Moq;
using TennisBookings.Web.Services;

namespace TennisBookings.Web.Tests.Controllers
{
    public class HomeControllerTests
    {
        [Fact]
        public void ReturnsExpectedViewModel_WhenWeatherIsSunny()
        {
            var mockWeatherForecaster = new Mock<IWeatherForecast>();
            mockWeatherForecaster.Setup(w => w.GetCurrentWeather())
                .Returns(new WeatherResult
                {
                    WeatherCondition = WeatherCondition.Sun
                });

            var sut = new HomeController(mockWeatherForecaster.Object);

            var result = sut.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<HomeViewModel>(viewResult.ViewData.Model);
            Assert.Contains("It's sunny right now. A great day for tennis."
                , model.WeatherDescription);
        }

        [Fact]
        public void ReturnsExpectedViewModel_WhenWeatherIsRainy()
        {
            var mockWeatherForecaster = new Mock<IWeatherForecast>();
            mockWeatherForecaster.Setup(w => w.GetCurrentWeather())
                .Returns(new WeatherResult
                {
                    WeatherCondition = WeatherCondition.Rain
                });

            var sut = new HomeController(mockWeatherForecaster.Object);

            var result = sut.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<HomeViewModel>(viewResult.ViewData.Model);
            Assert.Contains("We don't have the latest weather information right now" +
                            ", please check again later.", model.WeatherDescription);
        }
    }
}