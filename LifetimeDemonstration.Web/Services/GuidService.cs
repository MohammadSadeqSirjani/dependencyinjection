﻿using System;

namespace LifetimeDemonstration.Web.Services
{
    public class GuidService
    {
        public readonly Guid ServiceGuid;

        public GuidService()
        {
            ServiceGuid = Guid.NewGuid();
        }

        public string GetGuid() => ServiceGuid.ToString();
    }
}
